var animals = new Vue({
  el: "#app",
  data: {
    allAnimals: [], // empty array, data from JSON will be pushed into here
    animalTypes: [], // emty array, filter options will be pushed into here
    selectedAnimals: [],
    selectedAnimalTypes: []
  },
  mounted: function() {
    this.getData();
  },

  computed: {

    filteredAnimals: function() {

      var _this = this, // Assign to _this for scoping (mainly for use with forEach)
          returnVal = [];


      // Get filter options from JSON
      // In this example, that being ['Cat', 'Dog', 'Rabbit']
      if ( _this.animalTypes.length === 0 ) {
        _.forEach(_this.allAnimals.Filters, function(value, key) {
              
          _.forEach(value.Options, function(_value, _key) {
            _this.animalTypes.push(_value.Name)
          });

        });
      }


      // Filter by animal type
      _.forEach(_this.selectedAnimalTypes, function(value, key) {

        var chosenAnimalTypeExists = _.includes(_this.animalTypes, value),
            chosenAnimalType = value;

        if( chosenAnimalTypeExists ) {

          _.forEach(_this.allAnimals.Documents, function(_value, key) {
            if (_value.AnimalType === chosenAnimalType) {
              returnVal.push(_value);
            }
          });

        }

      });


      // Return either all documents, or the filtered documents
      if (returnVal.length === 0) {
        return _this.allAnimals.Documents;
      } else {
        return returnVal;
      }


    }

  },

  methods: {
    

    getData: function() {
      
      $.ajax({
        url: 'data.json'
      })
      .done(function(data) {
        animals.allAnimals = data; // 
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        console.log('Status code: ' + jqXHR.status);
        console.log('Error thrown: ' + errorThrown);
        console.log('Response text: ' + jqXHR.responseText);
      });

    },


    filterChangeHandler: function(el) {
      // $('.animal-pod').fadeOut
    }
     
  }
})